import pewgame as pew # setting up tools for the pewpew
import pygame
from QRogue import InteractiveCircuit

pew.init() # initialize the game engine...
screen = pew.Pix(30, 30) # ...and the screen
# brightness of pixel at (1,2) 
b = 0
# set brightness of the pixel
screen.pixel(1,2,b) 
pew.show(screen) # update screen to display the above

startX = 2
startY = 2
endX = 5
endY = 7
state = 3
currX = startX
currY = startY
circuitLoc = [900, 50]
histLoc = [900, 250]
textLoc = [900, 450]
doorStates = [1, 1, 1, 1]
circ = InteractiveCircuit()
# Get doors for first room
doorsString = circ.play()
doors = [bool(d) for d in doorsString]
# TODO: Draw circuit from png
#
# State 1 : Room being played - wait for enter input (stub for now)
# State 2 : Modify circuit - choose a gate from a list to add to the circuit
# State 3 : Choosing where to go - wait for valid arrow key, and then run a single shot to get the next room
circImage = pygame.transform.smoothscale(pygame.image.load('circuit.png'), (300,200))
histImage = pygame.image.load('histogram16.png')
histImage = pygame.transform.smoothscale(histImage, (300, 200))
imageRect = circImage.get_rect()
pew.showImg(circImage, *circuitLoc)
pew.showImg(histImage, *histLoc)
pew.showText('something', *textLoc)
while True: # loop which checks for user input and responds
    # TODO: Draw maze
    keys = pygame.key.get_pressed()
    
    if state == 1:
        # TODO: Write instructions
        if keys[pygame.K_ENTER]:
            state = 2
    elif state == 2:
        # TODO: Write instructions and numbered options
        
        if keys[pygame.K_1] | keys[pygame.K_2] | keys[pygame.K_3] | keys[pygame.K_4]:

            # Get keys and add gate accordingly
            pass
    elif state == 3:
        # TODO: Draw instructions to press an arrow key
        keys = pew.keys() # get current key presses
        if keys!=0:
            if keys&(pew.K_UP | pew.K_DOWN | pew.K_LEFT | pew.K_RIGHT):
                if pew.K_UP & doorStates[1]:
                    currY -= 1
                    state = 1 # Setting the state to room being played
                elif pew.K_DOWN & doorStates[3] :
                    currY += 1
                    state = 1 # Setting the state to room being played
                elif pew.K_RIGHT & doorStates[2]:
                    currX += 1
                    state = 1 # Setting the state to room being played
                elif pew.K_LEFT & doorStates[4]:
                    currX -= 1
                    state = 1 # Setting the state to room being played
                
                if state == 1: # Checking whether the input was valid
                    # TODO: Call play() method
                    doorStates = [1, 1, 1, 1]

                

    pew.showImg(circImage, *circuitLoc)
    pew.tick(1/6) # pause for a sixth of a second        

Collapse
#!/usr/bin/env python
# coding: utf-8

import time
import qiskit
import pewgame as pew # setting up tools for the pewpew
import pygame
import pickle
from qiskit.tools.visualization import plot_histogram

# ===========================================================================================================================
# QISKIT CIRCUIT ENCAPSULATION ==============================================================================================
# ===========================================================================================================================


class InteractiveCircuit:


    def __init__(self, levelname, size = 4, simulated = True):

        qr = qiskit.QuantumRegister(size)
        cr = qiskit.ClassicalRegister(size)

        # Initialisation part
        with open(levelname, "rb") as f:
            self.circuit_init = pickle.load(f)

        # Editable part
        self.circuit_main = qiskit.QuantumCircuit(qr, cr)

        # Measurement part
        self.circuit_mes = qiskit.QuantumCircuit(qr, cr)
        self.circuit_mes.measure(qr, cr)

        self.sim_backend = qiskit.Aer.get_backend("qasm_simulator")

        if not simulated:

            qiskit.IBMQ.load_account()
            provider = qiskit.IBMQ.get_provider(hub = "ibm-q")
            print("Available backends:")
            for backend in provider.backends():
                print('    - %s' % backend)
    
            self.real_backend = provider.get_backend("ibmqx4") # Only need 4 qubits for now

        self.simulated = simulated


    def probe(self, nshots = 10000):

        def gate_distribution(counts):

        ###for each qubit we compute the probability to have 0 and 1
            dic = {'0':[0,0],'1':[0,0],'2':[0,0],'3':[0,0]}

            for index,value in counts.items():

                for i in range(len(index)):

                    if(index[int(i)]=='0'):

                        dic[str(len(index)-int(i)-1)][0]+=value

                    elif(index[int(i)]=='1'):

                        dic[str(len(index)-int(i)-1)][1]+=value

            return dic

        def prob_open_door(counts):

            dict_counts=gate_distribution(counts)
            for key,value in dict_counts.items():
                dict_counts[key]=value[1]/((value[0]+value[1]))
            return dict_counts

        counts = qiskit.execute((self.circuit_init + self.circuit_main + self.circuit_mes), self.sim_backend, shots = nshots).result().get_counts()

        counts_reverted = {}

        for k in counts:
            counts_reverted[k[::-1]] = counts[k]

        plot_histogram(counts_reverted).savefig("histogram16.png")

        d = prob_open_door(counts)

        return counts, d


    def play(self):

        job = qiskit.execute((self.circuit_init + self.circuit_main + self.circuit_mes), self.sim_backend if self.simulated else self.real_backend, shots = 1)

        for k in job.result().get_counts().keys():
            return k


    def add_gate(self, gate_string, qubits):

        if gate_string.lower() == 'x':
            self.circuit_main.x(qubits)
        elif gate_string.lower() == 'z':
            self.circuit_main.z(qubits)
        elif gate_string.lower() == 'h':
            self.circuit_main.h(qubits)
        elif gate_string.lower() == 'cx':
            self.circuit_main.cx(*qubits)


    def export_circuit(self):

        diagram = (self.circuit_init + self.circuit_main).draw(output = "mpl")
        diagram.savefig("circuit.png", format = "png")

        return diagram


# ===========================================================================================================================
# MAIN GAME ENGINE ==========================================================================================================
# ===========================================================================================================================


pew.init() # initialize the game engine...
screen = pew.Pix(30, 30) # ...and the screen
# brightness of pixel at (1,2)
b = 0
# set brightness of the pixel
screen.pixel(1, 2, b)
pew.show(screen) # update screen to display the above

startX = 2
startY = 2
endX = 5
endY = 7
state = 3
currX = startX
currY = startY
gridLoc = [0, 0]
circuitLoc = [900, 0]
histLoc = [900, 500]
probtileLoc = [1320, 580]
gateTextLoc = [900, 850]
instLoc = [900, 750]
instSize = (300, 100)
tileside = 100

circ = InteractiveCircuit("Entangled_Rx.p")
background = pygame.transform.scale(pygame.image.load("background4.png"), (1500, 900))
tile = pygame.image.load("new_gates/grid.jpg")
probtile = pygame.transform.scale(pygame.image.load("new_gates/1111.png"), (tileside, tileside))
exitile = pygame.transform.scale(pygame.image.load("new_gates/exit.png"), (tileside, tileside))

move = pygame.image.load("move.png")
choseGate = pygame.image.load("use_gate2.png")
choseQubits = pygame.image.load("select_qubits.png")
arrows = pygame.image.load("arrows_circuit2.PNG")

# State 1 : Room being played - wait for enter input (stub for now)
# State 2 : Modify circuit - choose a gate from a list to add to the circuit
# State 3 : Choosing where to go - wait for valid arrow key, and then run a single shot to get the next room

gameOver = False

curpos = [300, 300]
exitpos = [500, 500]
qubitkeys = {pygame.K_UP : '0', pygame.K_LEFT : '1', pygame.K_DOWN : '2', pygame.K_RIGHT : '3'}
mapping = {0 : pygame.K_UP, 1 : pygame.K_LEFT, 2 : pygame.K_DOWN, 3 : pygame.K_RIGHT}
gate_list = {pygame.K_x : 'X', pygame.K_z : 'Z', pygame.K_h : 'H'}
velocity = {pygame.K_UP : [0, - tileside], pygame.K_LEFT : [-tileside, 0], pygame.K_DOWN : [0, tileside], pygame.K_RIGHT : [tileside, 0]}

while not gameOver: # loop which checks for user input and responds

    pew.showImg(background, 0, 0)

    for x in range(0, 700, tileside):
        for y in range(0, 700, tileside):
            pew.showImg(pygame.transform.scale(tile, (tileside, tileside)), x, y)

    pew.showImg(exitile, *exitpos)

    circ.export_circuit()
    circ.probe()

    circImage = pygame.transform.smoothscale(pygame.image.load('circuit.png'), (600, 500))
    histImage = pygame.transform.smoothscale(pygame.image.load('histogram16.png'), (300, 200))
    pew.showImg(circImage, *circuitLoc)
    pew.showImg(pygame.transform.smoothscale(arrows, (70, 300)), circuitLoc[0], circuitLoc[1] + 80)
    pew.showImg(histImage, *histLoc)
    pew.showImg(probtile, *probtileLoc)

    pew.showText(f"{int(circ.probe()[1]['0'] * 100)}%", probtileLoc[0] + tileside/2, probtileLoc[1] - 20, 20)
    pew.showText(f"{int(circ.probe()[1]['1'] * 100)}%", probtileLoc[0] - 50, probtileLoc[1] + tileside/2, 20)
    pew.showText(f"{int(circ.probe()[1]['2'] * 100)}%", probtileLoc[0] + tileside/2, probtileLoc[1] + 50 + tileside/2, 20)
    pew.showText(f"{int(circ.probe()[1]['3'] * 100)}%", probtileLoc[0] + 50 + tileside/2 + 10, probtileLoc[1] + tileside/2, 20)

    # State 1
    shot = circ.play()
    pew.showImg(pygame.transform.scale(pygame.image.load(f"new_gates/{shot}.png"), (tileside, tileside)), *curpos)
    pew.showImg(pygame.transform.scale(move, instSize), *instLoc)
    door_states = [bool(int(d)) for d in shot]
    available_directions = [mapping[i] for i,d in enumerate(door_states) if d]

    keyInput = len(available_directions) != 0
    while keyInput:

        for event in pygame.event.get():

            if event.type == pygame.KEYDOWN and event.key in available_directions:

                vel = velocity[event.key]
                curpos[0] += vel[0]
                curpos[1] += vel[1]
                
                if curpos == exitpos:
                    
                    pew.showText("YOU WON ! Thanks for playing QRogue !", *gateTextLoc, 20)
                    gameOver = True

                keyInput = False

    # State 2
    #TODO Insert gameplay events
    pew.showText(f"Available gates : {list(gate_list.values())}", *gateTextLoc, 20)
    pew.showImg(pygame.transform.scale(choseGate, instSize), *instLoc)

    keyInput = True
    while keyInput:

        for event in pygame.event.get():

            if event.type == pygame.KEYDOWN and event.key in gate_list.keys():

                chosen_gate = gate_list[event.key]

                keyInput = False

    pew.showImg(pygame.transform.scale(choseQubits, instSize), *instLoc)

    keyInput = True
    while keyInput:

        chosen_qubits = ''
        wanted_nqubits = 1

        for event in pygame.event.get():

            if event.type == pygame.KEYDOWN and event.key in qubitkeys.keys():

                chosen_qubits += qubitkeys[event.key]

                keyInput = len(chosen_qubits) < wanted_nqubits


    qubits = [int(q) for q in chosen_qubits]
    circ.add_gate(chosen_gate, qubits)
    
time.sleep(2)
